.. Maxe core documentation master file, created by
   sphinx-quickstart on Fri Aug 30 18:26:19 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Cratis
=====================================

Cratis is set of django tools integrated together to provide ready-to code
environment with all required tools:

- Easy command line tools
- DRY principle in project files and configuration
- Environment aware configuration (12factor pattern)
- Better looking admin UI
- Picture and file upload/management
- Cms integration
- Full i18n support out of the box
- Easy deployment

Cratis aim is to be highly modular, allowing to use any part of it on any
django-based application, and same easy remove it without any consequences.

Each feature of Cratis is a small building block, that does something useful.

Comand line tool
=====================================

Cratis command it is just, replacement for manage.py file in your project.
Now instead of `python manage.py` or `./manage.py` you can use just `cratis`.

Code of this command is very simple::

    cur_dir = os.getcwd()

    os.environ.setdefault("CRATIS_APP_PATH", cur_dir)
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cratis.settings")

    envdir_ = cur_dir + os.sep + 'envdir'
    if os.path.exists(envdir_):
        envdir.open(envdir_)

    execute_from_command_line(sys.argv)

What command does:

- Remembers current path where command was executed (later will be used in settings.py)
- Sets django settings module to cratis one

And executes standard things Django's manage.py usually does.

.. note:: Cratis sets only *default* value, so you can always set your own Django settings module and app_path.


Licence
=====================================

Cratis itself is MIT, but some dependencies use different licencing.
Django-suit that is used by Admin UI part of Cratis is free for commercial use,
so refer to djangosuit.com website to obtain licence for commercial projects.