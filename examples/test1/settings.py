# coding=utf-8
from cratis.features.admin import AdminThemeSuit, AdminArea
from cratis.features.debug import Debug
from cratis.features.django_cms import Cms
from cratis.features.filer import Filer, FilerCms
from cratis.features.i18n import I18n, I18nAdminUi
from cratis.features.themes import Theme
from cratis.settings import Dev as CratisDev


class Dev(CratisDev):

    FEATURES = (
        AdminThemeSuit(),
        AdminArea(),
        Debug(),

        I18n(langs=(
            ('ru', 'Russian'),
            ('en', 'English'),
        )),
        I18nAdminUi(),

        Cms(),

        Filer(),
        FilerCms(),

        Theme(name='foo')
    )

