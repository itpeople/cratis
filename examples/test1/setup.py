import os
from setuptools import setup, find_packages
# from tests import PyTest

setup(
    name='cratis-example-test1',
    version='0.1.0',
    packages=find_packages(),
    url='',
    license='MIT',
    author='Alex Rudakov',
    author_email='ribozz@gmail.com',
    description='Cratis test app',
    long_description=''
)

